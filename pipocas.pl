#!/usr/bin/perl
#Developed by ErroR
#Version 0.5
#Licence GPL3
use strict;
use warnings;
use LWP;
use WWW::Mechanize;
use XML::Simple;

my $mech = WWW::Mechanize->new();

&login();
sub login(){
	my $username = 'ErroR'; 
	my $password = 'belcross';
	my $url = "http://pipocas.tv/login.php";
	my $response = $mech->get($url);
    if(!$response->is_success){
        print "Pagina de login offline\n";
        exit(1);
    }else{
        $mech->form_name('login');
        $mech->field('username' => $username);
        $mech->field('password'=> $password);
        $mech->click();
        print "Login Efectuado\n";
       	&upload();

    }

}

sub upload(){
	my $upload_url = "http://pipocas.tv/enviar.php";
	my ($imdb,$release,$descricao,$ficheiro);
	my (%traducao,%lingua,%fps,%cds);
	### upload de legendas 
	print "A aceder a página de envio\n";
	my $response = $mech ->get('http://pipocas.tv/enviar.php');
	if(!$response->is_success){
		print "Página inacessivel\n";
		exit(1);
	}else{
		print "Página de envio acedida\n";	
	}

	print "A carregar informações de envio\n";
	open(FILE,"<","listagem.txt") || die $!;
	chomp(my @info = <FILE>);
	close(FILE);
	foreach my $data(@info){
		chomp $data;
	    my @list = split /;/, $data;
	    $ficheiro = "/home/rubem/Dropbox/Development_works/perl/preparing/".$list[2].".zip";
		$mech -> form_name('fr_enviar');
		$mech -> field('release'=> $list[0]);
		$mech -> field('imdb' => 'http://www.imdb.com/title/'.$list[1]);
		$mech -> field('descricao' => $list[3]);
		$mech -> field('ficheiro' => $ficheiro);
		if($list[4] =~ /(traducao|dvdrip|hdrip)/){ 
			$mech -> select('source' => $list[4]);
		}else{
			print "Formato da Origem da legenda está incorrecto";
			exit();
		}

		if($list[5] =~ /(portugues|brasileiro|ingles|espanhol)/){
			$mech -> select('linguagem' => $list[5]);
		}else{
			print "Linguagem especificada incorrecta";
			exit();
		}

		if($list[6] =~ /(23.976|23.980|24.000|25.000|29.970|30.000)/){
			$mech -> select('fps' => $list[6]);
		}else{
			print "Valor de FPS inválido";
			exit();
		}

		if($list[7] =~/(1|2|3|4|5|pack|N\/A)/){
			$mech -> select('cds' => $list[7]);
		}else{
			print "numero de cd's incorrectos";
			exit();
		}
		$mech -> submit();
		print "Upload efectuado com sucesso";
	}
}
